require 'rails_helper'

describe User do 

	before { @user = User.new(email: "example@example.com", password: "password", password_confirmation: "password", id: 1) }

	it "has a valid factory" do
		expect(@user).to be_valid
	end

	it "validates presence of email" do
		@user.email = nil
		expect(@user).to_not be_valid
	end

	it "is invalid with nil password" do
		@user.password = @user.password_confirmation = nil
		expect(@user).to_not be_valid
	end

	it "password doesn't match password_confirmation" do
		@user.password = "pass"
		expect(@user).to_not be_valid
	end

	it "hasn't default email notification" do 
		expect(@user.email_notification).to eq true
	end
end