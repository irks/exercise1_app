require 'rails_helper'

describe Post do 

	before { @user = User.new(email: "example@example.com", password: "password", password_confirmation: "password", id: 1)
			@post = Post.new(body:"123456787890", user_id: 1, id:1) }

	it "has a valid factories" do 
		expect(@user).to be_valid
		expect(@post).to be_valid
	end

	it "validates presence of body" do
		@post.body = nil
		expect(@post).to_not be_valid
	end

	it "has at least 10 characters" do
		@post.body = "21313"
		expect(@post).to_not be_valid
	end

	it "hasn't user_id" do
		@post.user_id = nil
		expect(@post).to_not be_valid
	end
end