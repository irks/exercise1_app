require 'rails_helper'

describe Comment do 

	before { @user = User.new(email: "example@example.com", password: "password", password_confirmation: "password", id: 1)
			@post = Post.new(body:"123456787890", user_id: 1, id:1)
			@comment = Comment.new(body: "123456", post_id: 1, user_id:1, id:1)}

	it "has a valid factories" do
		expect(@user).to be_valid
		expect(@post).to be_valid
		expect(@comment).to be_valid
	end

	context "validations of body, user_id, post_id" do
		it "has a validation of body presence" do
			@comment.body = nil
			expect(@comment).to_not be_valid
		end

		it "has a validation of only spaces in body" do
			@comment.body = "   "
			expect(@comment).to_not be_valid
		end

		it "has a validation of post_id presence" do
			@comment.post_id = nil
			expect(@comment).to_not be_valid
		end

		it "has a validation of user_id presence" do
			@comment.user_id = nil
			expect(@comment).to_not be_valid
		end

		it "has a validation of body length" do
			@comment.body = "n"*501
			expect(@comment).to_not be_valid
		end
	end

	context "destroy the post or user" do
		it ""
	end
end