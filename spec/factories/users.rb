FactoryGirl.define do 

	sequence(:email) {|n| "person-#{n}@example.com"}
	factory :user do |f|
		email
		password "password"
		password_confirmation "password"
	end
end