class Comment < ActiveRecord::Base
  belongs_to :post
  belongs_to :user
  validates :user_id, presence: true
  validates :body, presence: true, length: { maximum: 500 }
  validates :post_id, presence: true

end
