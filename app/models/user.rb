class User < ActiveRecord::Base
  
	has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy

  has_many :sent_invites, class_name: "Relationship", foreign_key: :inviting_id, dependent: :destroy
  has_many :received_invites, class_name: "Relationship", foreign_key: :invited_id, dependent: :destroy

  has_many :invited_users, through: :sent_invites, source: :invited_user
  has_many :inviting_users, through: :received_invites, source: :inviting_user 

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  def full_name
  		name.blank? ? email : name
  end

  def feed
    followed_ids = "SELECT invited_id FROM relationships
                     WHERE  inviting_id = :user_id"
    Post.where("user_id IN (#{followed_ids})
                     OR user_id = :user_id", user_id: id)
  end

  

  protected
  def confirmation_required?
  	false
  end

end
