class StaticPagesController < ApplicationController
	

	def home
		if current_user
			@feed_items = current_user.feed.order('created_at DESC').page(params[:page]).per(10)
		end
	end	

	
end
