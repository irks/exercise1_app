class CommentsController < ApplicationController
	before_filter :authenticate_user!, only: [:create, :destroy]

	def create
		@post = Post.find(params[:post_id])
		@comment = @post.comments.build(comment_params)
		@comment.user_id = current_user.id
		if @comment.save
			if (@comment.user_id != @post.user_id) && (@post.user.email_notification == true)
				UsersMailer.new_comment_email(@comment).deliver
			end
			flash[:success] = "Created new comment"
			redirect_to @post
		else
			flash[:danger] = "Something went wrong"
			redirect_to @post
		end
	end

	def destroy
		@post = Post.find(params[:post_id])
		@comment = @post.comments.find params[:id]
		if @comment.user_id == current_user.id
			@comment.destroy
			flash[:success] = "Comment deleted"
		else
			flash[:danger] = "You can't do this!"
		end
		redirect_to @post
		
	end


	private

	def comment_params
		params.require(:comment).permit(:post_id, :body)
	end
end
