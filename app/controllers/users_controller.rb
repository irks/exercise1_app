class UsersController < ApplicationController
	before_filter :authenticate_user!, only: [:index, :show]
	def index
		@users = User.order('created_at DESC').page(params[:page]).per(10)
	end

	def show
		@user = User.find(params[:id])
		@posts = @user.posts.order('created_at DESC').page(params[:page]).per(10)
	end
end
