class RelationshipsController < ApplicationController
	before_filter :authenticate_user!

	def create
	    @invited_user = User.find(params[:relationship][:invited_id])
	    if (@invited_user != current_user)
		    @relationship = current_user.sent_invites.build(invited_id: @invited_user.id)

		    if @relationship.save
		        flash[:success] = "Successfully invited"
		        redirect_to @invited_user
		    else
		        flash[:danger] = "Unsuccessful"
		        redirect_to users_path
		    end
		else
			redirect_to users_path
			flash[:danger] = "You cannot follow yourself"
		end
	end

	def destroy
	  @relationship = Relationship.find(params[:relationship][:relationship_id])
	  @inviting_user = User.find(@relationship.inviting_id)
	  if @inviting_user == current_user
	    @relationship.destroy
	    flash[:success] = "Removed relationship"
	  end
	  redirect_to @relationship.invited_user
	end

	def index
	    @sent_invites = current_user.sent_invites.order('created_at DESC').page(params[:page]).per(10)
	    @received_invites = current_user.received_invites.order('created_at DESC').page(params[:page]).per(10)
	end



end
