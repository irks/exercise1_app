class UsersMailer < ApplicationMailer
	default from: "irekwrobel23@gmail.com"

	def new_comment_email(comment)
		@comment = comment

		mail(to: comment.post.user.email, subject: "New Comment Notify")
	end

end
