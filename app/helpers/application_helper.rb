module ApplicationHelper

	# returns the full title
	def full_title(page_title = "")
		base_title = "Ruby on Rails Exercise App"
		if page_title.empty?
			base_title
		else
			page_title + " | " + base_title
		end
	end

	# checks if the post in the feed is writen by current_user
	def feed_post_style(post)
	    post.user_id == current_user.id ? "my_post" : "other_post"
  	end

end
